import Vue from 'vue'
import wrap from '@vue/web-component-wrapper';

import Time from './components/Time';
const BrmTime = wrap(Vue, Time);
window.customElements.define('brm-time', BrmTime);

import Date from './components/Date';
const BrmDate = wrap(Vue, Date);
window.customElements.define('brm-date', BrmDate);

/*
Vue.config.productionTip = false
new Vue({
  render: h => h(App),
}).$mount('#app')
*/
